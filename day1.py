#day 1
## variable and assignment
#valid
# 1. variable starting with char, var starting with underscore
# 2. var starting with char followed with number
#valid example
a=23
_b=34
a1=55
#############################
#@a=23
#note:prefer camel case for creating a variable
## assignment
# python supports multiple assignments 
a,b=23,45
print(a,b)
#############
#swap
a=13
b=14
(b,a)=(a,b)
print(a,b)
#############
#number system
a=23
b=23.34
c=(23+45j)
print(a,b,c)
print(type(a),type(b),type(c))
########################################
a='hello all'
b="evening"
c='''enjoy tea'''
d="""ready for RBG system"""
print(a,b,c,d,type(a))
#########################################
#list
list1=[1,2,3,4,5]
list2=[2,"hello",[2,3,4]]
print(list1)
print(list2)
print(type(list1))
#######################################
##tuple
tup1=(1,2,3,4,5)
tup2=(2,"hello",(2,4,3))
print(tup1)
print(tup2)
print(type(tup2))
######################################
##dictionary
dict1={"name":"atharva",
		"std":"SE",
		"roll no":61,}
print(dict1)
print(type(dict1))
####################################
##set
set1={1,2,3,4,5}
set2={2,4,5,6}
print(set1.issubset(set2))
print(set1)
print(set2)
print(type(set1))
##################################
##arithmetic
a=10
b=5
print(f"add :-{a+b},sub :-{a-b}")
print(f"multiply :-{a*b}")
#note :
#f""--f stands for format.Formats d output
a=3
b=2
print(a/b) #exact ans
print(a//b) # gives quotient
# a is -3 a//b gives -2 
#note if any operand in floor div is neg then output is floored value
a=10
b=3
print(a**b)
###########################################
##assignment
a=23
b=10
a+=b
print(a)
##########################################
##comparison
a=10
b=12
c=14
print(a>b)
print(a<=b)
print(a>=b)
print(a==c)
print(a<b>c)
########################################
##logical
a=0
b=23
c=45
print(a and b and c)
print(a or b or c)
print(a or b and b or c)
print(a-b or b-a or a-c or c-a)
######################################
##bitwise
a=5
b=10
print(a&b)
print(a|b)
print(b>>1)
###################################3
##membership
list1=[1,2,3,4,5]
a="hello world"
print(2 in list1)
print('w' in a)
print('world' not in a)
#####################################
##identity
a=23
b="23"
print(type(a) is type(b))
print(type(a) is not type(int(b)))
###################################
a=input("enter no")
print(a)
print(type(a))
################################
##code to find root
print(float(input("enter no"))**(1/float(input("enter root"))))
#####################################
##decision making
# if and else
a=23
b=34
if a>b:
	print("a is greater")
else :
	print("b is greater")	
print("a is greater") if(a>b) else print("b is greater")	
###########################################
##if and elif
a=10
b=15
c=20
if a>b and a>c:
	print("a is greater")
elif b>a and b>c:
	print("b is greater")
else :
	print("c is greater")	
##############################################
#note python does not support switch case
# print(range(1,10))
# print(list(range(1,10)))
# print(list(range(20)))
# print(list(range(0,100,2)))
# print(list(range(1,100,2)))
# print(list(range(100,1,-1)))
# print(list(range(99,0,-2)))
# #####
#####################################
# for i in "hello world":
# 	print(i)
# for i in "hello world":
# 	print(i,end=" ")

# for row in list(range(1,101)):
#     for col in list(range(1,11)):
# 		print(row*col,end=" ")
# 		print()
########################################
counter=0
while counter<=10:
	print("hello")
	counter+=1
else :	
	print("good bye")
################################
for i in "hello world":
		if i=="w":
				break
		print(i)			

for i in "hello world":
		if i=="w":
				continue
		print(i)					